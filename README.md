# maven-test

## How to use

1. Fork the project

2. Edit the URLs within the `pom.xml` to use your project ID

3. Copy the contents of `template_settings.xml` to your local `~/.m2/settings.xml` file.

4. Edit your `settings.xml` to use either A.) your local env var containing your API scoped PAT for GitLab.com, or replace the env var with your PAT in plain-text

5. Within the projects root directory, run the following commands:

```
mvn compile

mvn package

mvn deploy
```

Subsequent deploys will require you to adjust the version number within the `pom.xml`. 
